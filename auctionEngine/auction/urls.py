from django.urls import path, re_path
from django.conf.urls import url
from django.conf import settings
from django.views.static import serve
from django.views.generic import TemplateView
from django.contrib.auth.views import LoginView
from .forms.logintest_form import LoginForm
from .new_views import (argument_admin_view,
                        guideline_detail_admin_view,
                        category_admin_view, home,
                        login_admin_view,
                        checkrestapi_argument,
                        testman_ajax_views)

# app_name = 'auction'

urlpatterns = [
    # argument
    path('argument_create_form', argument_admin_view.getGuideline,
         name='argument_create_form'),
    path('argument_create',
         argument_admin_view.create,
         name='argument_create'),
    path('argument_edit', argument_admin_view.edit, name='argument_edit'),
    # category
    path('category_create',
         category_admin_view.create,
         name='category_createe'),
    path('category_edit', category_admin_view.edit, name='category_edit'),
    # guideline_detail
    path('guidelineDetail',
         guideline_detail_admin_view.read, name='guidelineDetail'),
    url(r'^guidelineDetail_delete/(\d+)',
        guideline_detail_admin_view.delete, name='guidelineDetail'),
    path('guideline_create_form',
         guideline_detail_admin_view.getCategory, name='getCategory'),
    path('guideline_create', guideline_detail_admin_view.create,
         name='guideline_create'),
    url(r'^guideline_edit_form/(\d+)',
        guideline_detail_admin_view.getData, name='guideline_edit_form'),
    #    path('checksave', guideline_detail_admin_view.checksave),
    path('guideline_edit',
         guideline_detail_admin_view.edit,
         name='guideline_edit'),

    # Main
    path('home', home.checkhtml, name='main'),

    # config static
    # url(r'^static/(?P<path>.*)$', serve,
    # {'document_root' : settings.STATIC_ROOT,}),
    # path('login', login_admin_view.login, name = 'login' ),
    # path('logintest', login_admin_view.login_view, name = 'loginnew' ),
    path('login', login_admin_view.login_view, name='loginnew'),
    path('signup', login_admin_view.signup, name='signupp'),
    # path('logout', login_admin_view.logout, name = 'logout'),
    path('logouttest', login_admin_view.logout_view, name='logoutnew'),
    path('page', TemplateView.as_view(template_name='admin/pagetest.html')),
    # permission
    path('test_permission',
         login_admin_view.permission_test,
         name='permission_test'),

    # api
    # path('api_argument', 
    # checkrestapi_argument.ListArgumentView, name = 'api_argument'),
    path('checkformset', checkrestapi_argument.checkform, name='check'),

    # testman - json
    # path('testajaxview', 
    # TemplateView.as_view(template_name = 'testman/testajax.html'),
    # name='ajaxview'),
    # path('testajax', testman_ajax_views.convert, name='testajax'),

    # guideline_agument_ajax
    path('newguideline',
         checkrestapi_argument.guideline_argument_ajax,
         name='gaa'),
    path('base', TemplateView.as_view(template_name='base.html')),
]
