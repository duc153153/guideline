from django import forms
from auction import models


class CreateForm(forms.Form):
    title = forms.CharField(max_length=255,
                            error_messages={'required': 'title please!!'})
    api_path = forms.CharField(max_length=255, required=False)
    code = forms.CharField(max_length=6000, required=False)
    category_id = forms.IntegerField()

    def clean_category_id(self):
        cid = self.cleaned_data['category_id']
        if models.GuidelineDetail.objects.filter(category=cid).exists():
            raise forms.ValidationError('exist!')
        return cid
