from django import forms
from auction import models


class GuidelineDetailForm(forms.Form):
    # id = forms.IntegerField()
    title = forms.CharField(max_length=255)
    api_path = forms.CharField(max_length=255, required=False)
    code = forms.CharField(max_length=1000, required=False)
    category_id = forms.IntegerField()

