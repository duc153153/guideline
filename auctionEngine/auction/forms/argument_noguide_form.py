from django import forms
from auction import models


class ArgumentForm(forms.Form):
    key = forms.CharField(max_length=255,
                          error_messages={'required': 'key please!!'})
    value = forms.CharField(max_length=1000,
                            error_messages={'required': 'value please!!'})
