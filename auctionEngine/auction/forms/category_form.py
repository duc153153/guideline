from django import forms
from auction import models
from django.core.exceptions import ValidationError


class CategoryForm(forms.Form):
    name = forms.CharField(max_length=255, error_messages={
                           'required': 'name please!!'})
    parent_id = forms.IntegerField(required=False)

    def clean_name(self):
        if models.Category.objects.filter(name=self.cleaned_data.get('name'))\
                                          .exists():
            raise forms.ValidationError('exist name!!')
        return self.cleaned_data.get('name')
