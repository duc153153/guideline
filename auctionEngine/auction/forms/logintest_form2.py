from django import forms
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError


class LogintestForm2(forms.Form):
    username = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255, widget=forms.PasswordInput)
