from django import forms
from auction import models


class Checkthis(forms.Form):
    key = forms.CharField(max_length=255)
    value = forms.CharField(max_length=1000)
    guidelineDetail_id = forms.IntegerField()
