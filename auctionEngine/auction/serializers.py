from rest_framework import serializers
from .models import Arguments, GuidelineDetail, Category


class CategorySeriallizers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('name')

class GuidelineDetailSeriallizers(serializers.ModelSerializer):
    category = CategorySeriallizers(many = True)

    class Meta:
        model = GuidelineDetail
        fields = ('title', 'api_path', 'code', 'category_id')

class ArgumentSeriallizers(serializers.ModelSerializer):
    guide = GuidelineDetailSeriallizers(many = True)

    class Meta:
        model = Arguments
        fields = ('key', 'value', 'guideline_detail')