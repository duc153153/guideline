from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length = 255)
    parent_id = models.IntegerField(blank=True, null=True, default='')
    objects = models.Manager()
    class Meta:
        db_table = 'category'

class GuidelineDetail(models.Model):
    title = models.CharField(max_length = 255)
    api_path = models.CharField(max_length = 255, blank = True)
    code = models.CharField(blank = True, max_length = 6000)
    category = models.OneToOneField('Category', on_delete = models.CASCADE)
    objects = models.Manager()
    class Meta:
        db_table = 'guideline_detail'

class Arguments(models.Model):
    key = models.CharField(max_length = 255)
    value = models.CharField(max_length = 255)
    subtext = models.CharField(max_length = 255, blank = True, default = '')
    guideline_detail = models.ForeignKey('GuidelineDetail', on_delete = models.CASCADE, default = 1)
    objects = models.Manager()
    class Meta:
        db_table = 'arguments'



class Account(models.Model):
    username = models.CharField(max_length = 255)
    password = models.CharField(max_length = 255)
    roles = models.ForeignKey('Roles', on_delete = models.PROTECT, default = 1)
    objects = models.Manager()
    class Meta:
        db_table = 'account'

class Roles(models.Model):
    name = models.CharField(max_length = 45)
    role = models.CharField(max_length = 45)
    objects = models.Manager()
    class Meta:
        db_table = 'roles'

# objects = models.Manager()
