from django.shortcuts import render
from auction.models import Category, Arguments, GuidelineDetail
# Create your views here.

def auction(request):
    category = Category.objects.all()
    arguments = Arguments.objects.get(guideline_detail = 0)
    guideline_detail = GuidelineDetail.objects.get(category = 0)

    return render(request, 'Auction Engine.html',{'category' : category, 'arguments' : arguments, 'guideline' : guideline_detail})

    


