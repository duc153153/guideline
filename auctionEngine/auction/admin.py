from django.contrib import admin
from auction import models
# Register your models here.
#register multiple models
mymodels = [models.Arguments, models.Category, models.GuidelineDetail]
admin.site.register(mymodels)