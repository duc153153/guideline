from django.shortcuts import render
from auction import models
from auction.forms.argument_form import ArgumentForm
from django.http import JsonResponse


# Guideline list
def getGuideline(request):
    guideline_detail = models.GuidelineDetail.objects.all()
    return render(request, 'admin/argument_create.html',
                  {'guideline': guideline_detail})

# Create


def create(request):
    status = False
    argument = models.Arguments()
    guideline_detail = models.GuidelineDetail.objects.all()
    argument_data = ArgumentForm(request.POST)
    print(request.POST)
    if request.method == "POST":
        if argument_data.is_valid():
            argument.key = argument_data.cleaned_data['key']
            argument.value = argument_data.cleaned_data['value']
            argument.guideline_detail = models.GuidelineDetail.objects.get(
                pk=argument_data.cleaned_data['guideline_detail'])
            argument.save()
            status = True
            return render(request, 'admin/argument_create.html',
                          {'status': status, 'guideline': guideline_detail})
        else:
            return render(request,
                          'admin/argument_create.html',
                          {'status': status,
                           'form': argument_data,
                           'guideline': guideline_detail})
    else:
        return render(request, 'admin/argument_create.html',
                      {'status': status, 'guideline': guideline_detail})


# Edit
def edit(request):
    status = False
    argument_data = ArgumentForm(request.POST)
    guideline_detail = models.GuidelineDetail.objects.all()
    if request.method == "POST":
        if argument_data.is_valid():
            id = argument_data.cleaned_data['id']
            argument = models.Arguments.objects.get(pk=id)
            argument.key = argument_data.cleaned_data['key']
            argument.value = argument_data.cleaned_data['value']
            argument.guideline_detail = \
                argument_data.cleaned_data['guideline_detail']
            argument.save()
            status = True
            return render(request, 'admin/argument_edit.html',
                          {'status': status, 'guideline': guideline_detail})
        else:
            return render(request, 'admin/argument_edit.html',
                          {'status': status, 'form': argument_data,
                           'guideline': guideline_detail})
    else:
        return render(request, 'admin/argument_edit.html', {'status': status})
