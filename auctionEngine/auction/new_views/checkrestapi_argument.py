from rest_framework import generics
from auction import models, serializers, forms
from auction.forms.checkform import Checkthis
from auction.forms.guide_create_form import CreateForm
from auction.forms.argument_noguide_form import ArgumentForm
from django.forms.formsets import formset_factory
from django.forms import inlineformset_factory, modelformset_factory
from django.shortcuts import render
from django.views.generic import CreateView
from django.http import JsonResponse
import json


def checkform(request):
    argumentFormset = inlineformset_factory(models.GuidelineDetail,
                                            models.Arguments,
                                            fields=['key', 'value'],
                                            extra=2,
                                            can_delete=True,
                                            can_order=True)
    if request.method == 'POST':
        form = forms.guidelineDetailForm.GuidelineDetailForm(request.POST)
        formset = argumentFormset(request.POST or None)
        if form.is_valid() and formset.is_valid():
            guide = models.GuidelineDetail()
            # guide = form.save(commit=False)
            guide.title = form.cleaned_data['title']
            guide.api_path = form.cleaned_data['api_path']
            guide.code = form.cleaned_data['code']
            guide.category = models.Category.objects.get(
                pk=form.cleaned_data['category_id'])
            guide.save()

            for f in formset:
                try:
                    argument = models.Arguments()
                    argument.key = f.cleaned_data['key']
                    argument.value = f.cleaned_data['value']
                    argument.guideline_detail = guide
                    argument.save()
                except Exception as e:
                    break
    else:
        form = forms.guidelineDetailForm.GuidelineDetailForm()
        formset = argumentFormset()
    context = {
        'form': form,
        'formset': formset,
    }
    return render(request, 'admin/checkguideline.html', context)


def guideline_argument_ajax(request):
        # print(request.POST.get('guideline'))
    category_guide = models.Category.objects.all()
    data2 = request.POST.get('guideline')
    data3 = request.POST.get('argument')

    data = {
        'check': 'ok',
    }
    location = []
    list_error_ar = []
    if request.method == "POST":
        argument_status = 1
        guideline_status = 0
        if data2 and data3:
            dat2 = json.loads(data2)
            dat3 = json.loads(data3)
            guideline_data = forms.guidecreateForm.CreateForm(dat2)
            guide = models.GuidelineDetail()

            # check argument
            for num in range(0, len(dat3)):
                argument_data = ArgumentForm(dat3[num])
                if not argument_data.is_valid():
                    argument_status = 0
                    break

            if guideline_data.is_valid():
                guideline_status = 1
            else:
                guideline_status = 0

            if argument_status == 0 or guideline_status == 0:
                print('no valid')
                if argument_status == 0:
                    for num in range(0, len(dat3)):
                        argument_data = ArgumentForm(dat3[num])
                        if not argument_data.is_valid():
                            location.append(num)
                            if 'value' not in argument_data.errors:
                                list_error_ar.append({
                                        'key': argument_data.errors['key'][0],
                                        'value': ''
                                })
                            elif 'key' not in argument_data.errors:
                                list_error_ar.append({
                                    'key': '',
                                    'value': argument_data.errors['value'][0]
                                })
                            else:
                                list_error_ar.append({
                                    'key': argument_data.errors['key'][0],
                                    'value': argument_data.errors['value'][0]
                                })
                            data['argument_error'] = list_error_ar
                            data['argument_location_error'] = location
                            print(list_error_ar)
                            print(data['argument_error'])
                            print(data['argument_location_error'])
                        else:
                            list_error_ar.append({
                                    'key': '',
                                    'value': ''
                            })

                if guideline_status == 0:
                    if not guideline_data.is_valid():
                        data['guideline_error'] = guideline_data.errors

                return JsonResponse(data)
            else:

                # guideline
                print('api is: %s' % guideline_data.cleaned_data['api_path'])
                if guideline_data.cleaned_data['api_path'] == '':
                    guide.api_path = 'None'
                else:
                    guide.api_path = guideline_data.cleaned_data['api_path']
                    print('valid')
                guide.title = guideline_data.cleaned_data['title']
                guide.code = guideline_data.cleaned_data['code']
                guide.category = models.Category.objects.get(
                    pk=guideline_data.cleaned_data['category_id'])
                guide.save()
                # argument
                for ar in range(0, len(dat3)):
                    dat3[ar]['guideline_detail'] = guide
                    argument_data = ArgumentForm(dat3[ar])
                    if argument_data.is_valid():
                        print('check... ' + str(guide))
                        print(dat3[ar]['key'])
                        print(guide)
                        argument = models.Arguments()
                        argument.key = dat3[ar]['key']
                        argument.value = dat3[ar]['value']
                        argument.subtext = dat3[ar]['subtext']
                        argument.guideline_detail = guide
                        argument.save()
                return JsonResponse(data)
            return JsonResponse(data)
        else:
            data['check']: 'fail'
            return JsonResponse(data)
    return JsonResponse(data)
