from django.shortcuts import render, redirect
from django.http import HttpResponse
from auction import models
from auction.forms.login_form import Login
from auction.forms.logintest_form2 import LogintestForm2
from auction.new_views.guideline_detail_admin_view import read
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import Permission, User


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse('ok sign up!')
    else:
        form = UserCreationForm()
    return render(request, 'admin/signup.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect(login_view)


def login_view(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('/auction/home')
        else:
            return render(request, 'registration/login.html',
                          {'error': 'Not found!!'})
    return render(request, 'registration/login.html')


def permission_test(request):
    user = User.objects.get(username='testman')
    check_permission = Permission.objects.get(name='Can view category')
    user.user_permissions.add(check_permission)
    # set to staff
    # user.is_staff=False
    # user.is_admin = True
    # user.is_superuser = True
    # user.save()

    # Check perm2
    print(user.get_all_permissions())
    # Check2 = Permission.objects.filter(user=user)
    return HttpResponse('permission testing!!!')
