from django.shortcuts import render, redirect
from auction import models
from auction.forms.guideline_detail_form import GuidelineDetailForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, permission_required


@login_required(login_url='/auction/login')
@permission_required('auction.view_category', login_url='/auction/login')
def read(request):
    guideline_detail_list = models.GuidelineDetail.objects.all()
    return render(request, 'admin/guidelineDetail.html',
                  {'guidelineDetail': guideline_detail_list})

# delete


def delete(request, id):
    delStatus = False
    guideline = models.GuidelineDetail.objects.get(pk=id)
    guideline.delete()
    delStatus = True
    guideline_detail_list = models.GuidelineDetail.objects.all()
    return render(request,
                  'admin/guidelineDetail.html',
                  {'guidelineDetail': guideline_detail_list,
                   'delStatus': delStatus})

# create


def create(request):
    status = False
    guideline = models.GuidelineDetail()
    categoryGuide = models.Category.objects.all()
    guideline_data = GuidelineDetailForm(request.POST)
    if not request.POST.get('api_path'):
        guideline.api_path = 'None'

    if request.method == "POST":
        # print(request.POST['category_id']+request.POST['title']+request.POST['api_path']+request.POST['code'])
        if guideline_data.is_valid():
            guideline.title = guideline_data.cleaned_data['title']
            if not guideline_data.cleaned_data['api_path']:
                guideline.api_path = 'None'
            else:
                guideline.api_path = guideline_data.cleaned_data['api_path']
                print(guideline.api_path)
                guideline.code = guideline_data.cleaned_data['code']
                guideline.category = models.Category.objects.get(
                    pk=guideline_data.cleaned_data['category_id'])
                guideline.save()
                status = True

                return render(request, 'admin/guideline_create.html',
                              {'status': status, 'category': categoryGuide})
        #     return redirect(getCategory)
        #     return render(request, 'admin/guideline_create.html',
        #                   {'status':status})
        else:
            #     guideline_data = guideline_detail_form()
            return render(request, 'admin/guideline_create.html',
                          {'status': status,
                           'category': categoryGuide,
                           'form': guideline_data})
    else:
        return render(request, 'admin/guideline_create.html',
                      {'status': status, 'category': categoryGuide})

# get category


def getCategory(request):
    category = models.Category.objects.all()
    return render(request, 'admin/guideline_create.html',
                  {'category':  category})

# edit
# editform data


def getData(request, id):
    guideline = models.GuidelineDetail.objects.get(pk=id)
    categoryData = models.Category.objects.all()
    return render(request, 'admin/guideline_edit.html',
                  {'guideline': guideline, 'category': categoryData})


def edit(request):
    editStatus = False
    guideline_data = GuidelineDetailForm(request.POST)
    guideline_detail_list = models.GuidelineDetail.objects.all()
    if request.method == "POST":
        if guideline_data.is_valid():
            guideline = models.GuidelineDetail.objects.get(
                pk=request.POST['id'])
            guideline.title = guideline_data.cleaned_data['title']
            guideline.api_path = guideline_data.cleaned_data['api_path']
            guideline.code = guideline_data.cleaned_data['code']
            id = guideline_data.cleaned_data['category_id']
            guideline.category = models.Category.objects.get(pk=id)
            guideline.save()
            editStatus = True
            return render(request, 'admin/guidelineDetail.html',
                          {'editStatus': editStatus,
                           'guidelineDetail': guideline_detail_list})
        else:
            guide = models.GuidelineDetail()
            guide.title = request.POST.get('title')
            guide.api_path = request.POST.get('api_path')
            guide.code = request.POST.get('code')
            guide.category = models.Category.objects.get(
                pk=request.POST.get('category_id'))
            guide.id = request.POST.get('id')
            categoryData = models.Category.objects.all()
            return render(request, 'admin/guideline_edit.html',
                          {'editStatus': editStatus,
                           'guideline': guide,
                           'form': guideline_data,
                           'category': categoryData})
    else:
        return render(request, 'admin/guideline_edit.html',
                      {'editStatus': editStatus})

