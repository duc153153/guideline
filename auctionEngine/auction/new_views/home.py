from django.shortcuts import render
from auction import models, new_views
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ObjectDoesNotExist

# sidebar

# content

# checkhtml


def checkhtml(request):

    # check child - parent
    category_data = models.Category.objects.all()
    status = True
    no_parent = False
    if category_data:
        id_parent = request.GET.get('parent')
        if id_parent:
            print(request.GET.get('parent'))
            child_id = request.GET.get('id')
            child_data = models.Category.objects.filter(parent_id=id_parent)

            try:
                guideline = models.GuidelineDetail.objects.get(
                    category=models.Category.objects.get(pk=child_id))
                argument = models.Arguments.objects.filter(
                    guideline_detail=guideline.id)
                print(child_data)
                no_parent = True
                return render(request, 'blank.html',
                              {'category': category_data,
                               'child': child_data, 'guideline': guideline,
                               'argument': argument,
                               'status': status,
                               'no_parent': no_parent})
            except ObjectDoesNotExist:
                no_parent = True
                status = False
                return render(request, 'blank.html',
                              {'category': category_data,
                               'child': child_data,
                               'status': status,
                               'no_parent': no_parent})

        else:
            no_parent = False
            status = True
            ##
            # family = []
            # parent = []
            # children = []
            # for c in category_data:
            #     if c.parent_id is None:
            #         parent.append(
            #             {
            #                 'id': c.id,
            #                 'name':c.name,
            #                 'parent_id':c.parent_id
            #             }
            #         )
            #     for num in range(0,len(parent)):
            #         if parent[num]['id'] == c.id:
            #             child = models.
            #               Category.objects.filter(parent_id = c.parent_id)
            #             for child in child:
            #                     children.append(
            #                         {
            #                             'id': child.id,
            #                             'name':child.name,
            #                             'parent_id':child.parent_id
            #                         }
            #                     )
            #     family.append({
            #         'parent':parent,
            #         'children':children
            #     })
            # print(parent)
            # print(children)
            # print(family[0]['parent'])
            # print(len(family[0]['parent']))
            # for i in family:
            #     print(i['parent'][0]['id'])
            ##
            guideline = models.GuidelineDetail.objects.all().first()
            argument = models.Arguments.objects.filter(
                guideline_detail=guideline.id)
            return render(request, 'blank.html',
                          {'category': category_data,
                           'guideline': guideline,
                           'argument': argument,
                           'status': status,
                           'no_parent': no_parent})
