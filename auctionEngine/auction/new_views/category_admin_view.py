from django.shortcuts import render
from auction import models
from auction.forms.category_form import CategoryForm
from django.contrib.auth.decorators import permission_required

# Category list
def category():
    category_data = models.Category.objects.all()
    return category_data

# Create
def create(request):
    status = False
    category = models.Category()
    create_data = CategoryForm(request.POST)
    category_data = models.Category.objects.all()
    if request.method == 'POST':
        if create_data.is_valid():
            category.name = create_data.cleaned_data['name']
            category.parent_id = create_data.cleaned_data['parent_id']
            category.save()
            status = True
            return render(request, 'admin/category_create.html',
                          {'category_data': category_data, 'status': status})
        else:
            return render(request, 'admin/category_create.html',
                          {'category_data': category_data, 'status': status,
                           'form': create_data})
    else:
        return render(request, 'admin/category_create.html',
                      {'category_data': category_data,
                       'status': status})

# Edit
def edit(request):
    status = False

    if request.method == 'POST':
        category_data = CategoryForm(request.POST)
        if category_data.is_valid():
            id = category_data.cleaned_data['id']
            category = models.Category.objects.get(pk=id)
            category.name = category_data.cleaned_data['name']
            category.save()
            status = True

            return render(request, 'admin/category_edit.html',
                          {'status': 'true'})
    else:

        return render(request, 'admin/category_edit.html', {'status': status})
