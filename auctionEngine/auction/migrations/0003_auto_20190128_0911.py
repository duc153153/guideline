# Generated by Django 2.1.5 on 2019-01-28 09:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0002_category_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guidelinedetail',
            name='api_path',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
