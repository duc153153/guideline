# Generated by Django 2.1.5 on 2019-01-28 09:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0003_auto_20190128_0911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guidelinedetail',
            name='api_path',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
